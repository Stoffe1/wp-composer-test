<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Wp-content dir path and url ** //

define('WP_CONTENT_DIR', dirname(__FILE__) . '/wp-content');
define('WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/wp-content');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'comp');
define('DB_USER', 'root');
define('DB_PASSWORD', 'root');
define('DB_HOST', '127.0.0.1');
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the{@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'AoW:bg<4+/2t#uE@OA[l!/zi>!G9+)`bAqr:dfk[o,GFgpB[`P~:sZqRr/xbcvb=');
define('SECURE_AUTH_KEY', 'S7N/Qakp8?+6+@`Mh#9T[H`~Dle<=grq`+7;| 2T.+W E~c4)OKZ|O$+]S hcu{c');
define('LOGGED_IN_KEY', '^eaB:)PRAPN^<r{eE]:{QFnZGqkxTe*K-Jo{r+~d?iQ$Oz{L#Q&ce22`6:v-:)qM');
define('NONCE_KEY', 'B<a&27J&Ubei<c*TGF}|vr2n^Zzmr[s3]p{r$c$+@-7xN{~CCk?l32v;|=x@VU}J');
define('AUTH_SALT', 'J]%i1V66Qgk^PswDSu%BOnZfbPg5Vk.V/c[gY73ECHyFb/lifqWVg|8V%6+h6-x,');
define('SECURE_AUTH_SALT', '1f#t&$]2jp<hYX4,oe]X,a;5QZc/g?[nN2;2lPD4Y^*~)jN2=Xe(cQp&4!hqL7=1');
define('LOGGED_IN_SALT', 'R}qK-vn9#?k>%Ne]M`LHx`ifJc4u:jIRcD7%j%BnMeN?U0-u|x%{<-a@1?c|;nFv');
define('NONCE_SALT', '^tEmiT,/Beh?3z^HZ[-;MFeR.IF1&S,E)|.^+9BWL|6dGnkUR<)]&+t&b. vIq>I');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/');
}

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
